import json
import os
import re
from urllib.parse import parse_qs, urlparse

import googleapiclient.discovery
import googleapiclient.errors
import requests
from unidecode import unidecode

from youtube_utils.ct_tracks import (format_track_default, format_track_re,
                                     format_tracks_gpt)
from youtube_utils.get_dj import get_dj_from_youtube_title

scopes = ["https://www.googleapis.com/auth/youtube.readonly"]


class YoutubeManager:
    def __init__(self):
        api_service_name = "youtube"
        api_version = "v3"
        api_key = os.environ.get("YOUTUBE_API_KEY", "")
        self.yt = googleapiclient.discovery.build(
            api_service_name,
            api_version,
            developerKey=api_key,
            cache_discovery=False,
        )

    # GET VIDEO ID FROM URL
    @staticmethod
    def get_video_id(url):
        if "https://youtu.be/" in url:
            return url[url.rfind("/") + 1 :]
        else:
            url_data = urlparse(url)
            query = parse_qs(url_data.query)
            return query["v"][0]

    # GET VIDEO INFORMATION
    def get_video_metadata(self, video_id):
        request = self.yt.videos().list(part="snippet, statistics", id=video_id)
        response = request.execute()
        v = response["items"][0]

        try:
            likes = v["statistics"]["likeCount"]
        except Exception:
            likes = 0

        vdict = {
            "id": v["id"],
            "name": v["snippet"]["title"],
            "channel": {
                "id": v["snippet"]["channelId"],
                "name": v["snippet"]["channelTitle"],
            },
            "views": v["statistics"]["viewCount"],
            "likes": likes,
            "himg": v["snippet"]["thumbnails"],
            "description": v["snippet"]["description"],
        }

        return vdict

    @staticmethod
    def get_dj_from_youtube(title):
        dj_list = get_dj_from_youtube_title(title)
        return dj_list

    # GET YOUTUBE AND COMMENT TRACKS
    def get_youtube_tracks(self, video_metadata, method="both"):
        """Fetch YouTube tracks based on the given method: default, regex, or both."""

        video_id = video_metadata["id"]
        ncomments = 50
        tcomments = self._get_tcomments_from_comments(video_id, ncomments)

        tracks = {
            "youtube_tracks": self._get_tracks_from_youtube(
                video_id
            )  # YouTube tracks remain consistent across methods
        }

        # If method is both or regex, get tracks cleaned with regex
        if method in ["both", "regex"]:
            tracks["comment_tracks_regex"] = self._get_tracks_from_tcomments(tcomments, "regex")
            tracks["description_tracks_regex"] = self._get_tracks_from_description(
                video_metadata["description"], "regex"
            )

        # If method is both or default, get tracks cleaned with default
        if method in ["both", "default"]:
            tracks["comment_tracks_default"] = self._get_tracks_from_tcomments(tcomments, "default")
            tracks["description_tracks_default"] = self._get_tracks_from_description(
                video_metadata["description"], "default"
            )

        return tracks

    # GET YOUTUBE TRACKS (auto-detected)
    def _get_tracks_from_youtube(self, video_id) -> list[dict]:
        url = f"https://www.youtube.com/watch?v={video_id}"
        try:
            page = requests.get(url).text
            jtracks = self._get_jtracks_from_html(page)
            tracks = self._get_tracks_from_jtracks(jtracks)
            return tracks

        except Exception as e:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(e).__name__, e.args)
            print(message)
            return []

    @classmethod
    def _get_jtracks_from_html(cls, page, test_decode=False):
        decode = cls._get_decode()
        if decode or test_decode:
            page = page.encode("utf-8").decode("unicode_escape")
            page = page.encode("latin1").decode("utf8")
        start = page.find("carouselLockups")
        start_offest = page.find("[", start)
        finish = page.find("previousButton", start)
        page2 = page[start_offest:finish]
        finish_offest = page2.rfind("]")
        tracks = page2[: finish_offest + 1]
        tracks = cls.attempt_fix_json(tracks)
        tracks = cls.fix_invalid_escapes(tracks)
        tracks_json = json.loads(tracks)
        return tracks_json

    @classmethod
    def _get_tracks_from_jtracks(cls, tracks_json):
        tracks = []
        for jtrack in tracks_json:
            raw_track = cls._parse_json_tracks(jtrack)
            track = cls._format_yt_track(raw_track)
            tracks.append(track)
        return tracks

    @staticmethod
    def _parse_json_tracks(jtrack):
        # Title
        jtitle = jtrack["carouselLockupRenderer"]["videoLockup"]["compactVideoRenderer"]["title"]
        if jtitle.get("runs", None):
            title = jtitle["runs"][0]["text"]
        elif jtitle.get("simpleText", None):
            title = jtitle["simpleText"]
        else:
            raise Exception("Title not found")
        # Artist
        try:
            artist = jtrack["carouselLockupRenderer"]["infoRows"][0]["infoRowRenderer"][
                "defaultMetadata"
            ]["simpleText"]
        except KeyError:
            artist = "Exception parsing artist"
        # Album
        try:
            album = jtrack["carouselLockupRenderer"]["infoRows"][1]["infoRowRenderer"][
                "defaultMetadata"
            ]["simpleText"]
        except KeyError:
            album = "Exception parsing album"
        # Final track
        track = {"title": title, "artist": artist, "album": album}
        return track

    @staticmethod
    def _format_yt_track(track):
        return f'{track["title"]} {track["artist"].replace(", ", " ")}'

    # GET CT TRACKS (comments)
    def _get_tcomments_from_comments(self, video_id, ncomments=50):
        raw_comments = self._get_comments_from_video(video_id, maxResults=ncomments)
        tcomments = self._get_tcomments_from_raw_comments(raw_comments)
        return tcomments

    def _get_comments_from_video(self, video_id, maxResults):
        try:
            request = self.yt.commentThreads().list(
                part="snippet", maxResults=maxResults, order="relevance", videoId=video_id
            )
            res = request.execute()
            comment_items = res["items"]
            comments = [
                x["snippet"]["topLevelComment"]["snippet"]["textOriginal"] for x in comment_items
            ]
            return comments
        except Exception as e:
            template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            message = template.format(type(e).__name__, e.args)
            print(message)
            return []

    @classmethod
    def _get_tcomments_from_raw_comments(cls, comments):
        tcomments = []
        for comment in comments:
            tracklist = cls._return_possible_tracklist(comment.split("\n"))
            if tracklist:
                tcomments.append(tracklist)
        return tcomments

    @staticmethod
    def _filter_comment(comment):
        threshold = 5
        if comment.count("\n") > threshold:
            return True
        else:
            return False

    # FILTER LINES FROM CT TRACKS
    @classmethod
    def _get_tracks_from_tcomments(cls, tcomments, clean_style="default"):
        result = []
        for ct_tracks in tcomments:
            tracks = []
            if clean_style != "gpt":
                for ct_track in ct_tracks:
                    track = cls._format_ct_track(ct_track, clean_style=clean_style)
                    if track:  # Exclude None or empty strings
                        tracks.append(track)
                result.append(tracks)
            else:
                tracks = format_tracks_gpt(ct_tracks)
                if tracks:
                    result.extend(tracks)

        if result:  # If result is not an empty list
            return max(result, key=len)
        else:
            return []

    # FORMAT CT TRACKS
    @classmethod
    def _format_ct_track(cls, ct_track, clean_style="default"):
        if clean_style == "default":
            track = format_track_default(ct_track)
            # return None
        elif clean_style == "regex":
            track = format_track_re(ct_track)
        else:
            raise Exception("Invalid clean style")
        track = unidecode(track)
        return track

    # DESCRIPTION TRACKS
    @classmethod
    def _get_tracks_from_description(cls, description, ct_track="default"):
        result = []
        # if description is a list, don't do split
        if isinstance(description, list):
            dtracks = description[0]
        else:
            dtracks = description.split("\n")
        # check tracklist
        tracklist = cls._return_possible_tracklist(dtracks)
        if tracklist:
            if ct_track != "gpt":
                for dtrack in tracklist:
                    track = cls._format_ct_track(dtrack, clean_style=ct_track)
                    if track:
                        result.append(track)
            else:
                result = format_tracks_gpt(tracklist)

        return result

    @classmethod
    def _return_possible_tracklist(cls, lines):
        chars = ["-", "."]
        N = 5
        all_groups = []
        counts = 0
        valid_lines = []
        for line in lines:
            if cls._skip_line(line):
                continue
            if any(char in line for char in chars):
                counts += 1
                valid_lines.append(line)
            else:
                if counts > N:
                    all_groups.append(valid_lines)
                counts = 0
                valid_lines = []
        if counts > N:
            all_groups.append(valid_lines)

        if not all_groups:  # if no groups found
            return []

        # return the group with the most lines
        return max(all_groups, key=len)

    @staticmethod
    def _skip_line(line):
        skip_list = ["www", "http", ".com"]
        return any(sline in line for sline in skip_list)

    ### FIX JSON  METHODS ### (_get_jtracks_from_htmtl)
    @staticmethod
    def _get_decode():
        env = os.environ.get("ENV", "")
        if env == "docker":
            return True
        else:
            return False

    @classmethod
    def fix_invalid_escapes(cls, json_string):
        # Match any backslash followed by a character
        pattern = r"(\\.)"
        result = re.sub(pattern, cls.fix_match, json_string)
        return result

    @staticmethod
    def fix_match(match):
        escape_sequences = {r"\"", r"\\", r"\/", r"\b", r"\f", r"\n", r"\r", r"\t"}
        if match.group(1) not in escape_sequences:
            # This is not a valid escape sequence, replace with double backslash
            return r"\\"
        else:
            # This is a valid escape sequence, return as is
            return match.group(1)

    @staticmethod
    def attempt_fix_json(text):
        text = list(text)  # Convert string to list for mutable operations
        in_quote = False

        quotes = []

        for i in range(len(text) - 1):
            if text[i] == '"':
                if in_quote:  # this is a closing quote
                    if text[i + 1] not in [":", "}", ","]:
                        text[i] = "@"
                    else:
                        in_quote = False
                else:  # this is an opening quote
                    if text[i + 1] in [":", "}"]:
                        text[i] = "@"
                    else:
                        in_quote = True

        # Convert the list back to string
        text = "".join(text)
        return text

    @staticmethod
    def fix_json_error(json_string, error):
        error_pos = error.doc.index("}") + 1
        fixed_json_string = json_string[:error_pos] + "," + json_string[error_pos:]
        return fixed_json_string

    # # CHANNEL METHODS

    # @staticmethod
    # def get_channel_id(url):
    #     return url[url.rfind("/") + 1 :]

    # def get_channel(self, channelUrl):
    #     channelId = self.get_channel_id(channelUrl)
    #     channel_metadata = {
    #         "video_list": self.get_tracks_from_channel(channelId),
    #         "channel_info": self.get_channel_metadata(channelId),
    #     }

    #     return channel_metadata

    # def get_tracks_from_channel(self, channelId):
    #     video_list = self.get_videos_from_channel(channelId)
    #     for video_id, video in video_list.items():
    #         try:
    #             video.update(self.get_tracks_from_video(video_id))
    #         except:
    #             print(video_id)

    #     return video_list

    # def get_videos_from_channel(self, channelId):
    #     video_list = {}
    #     pageToken = None

    #     while True:
    #         request = self.yt.search().list(
    #             part="snippet",
    #             channelId=channelId,
    #             order="date",
    #             maxResults="50",
    #             type="video",
    #             pageToken=pageToken,
    #         )
    #         response = request.execute()
    #         pageToken = response.get("nextPageToken", None)
    #         video_list.update(
    #             dict.fromkeys([x["id"]["videoId"] for x in response["items"]])
    #         )
    #         if pageToken == None:
    #             break

    #     return video_list

    # def get_channel_metadata(self, channelId):
    #     request = self.yt.channels().list(part="snippet,statics", id=channelId)
    #     response = request.execute()
    #     c = response["items"][0]

    #     cdict = {
    #         "id": channelId,
    #         "title": c["snippet"]["title"],
    #         "description": c["snippet"]["description"],
    #         "subs": c["statics"]["subscriberCount"],
    #     }

    #     return cdict
