from my_utils.myutils import read_csv
from youtube_utils.ytutils import YoutubeManager


def test_get_tracks_from_yt_youtube():
    """Test get youtube tracks (comments and description tests missing)"""
    video = "https://www.youtube.com/watch?v=IIbcGjZy6OM"
    youtube = YoutubeManager()
    video_id = youtube.get_video_id(video)
    video_metadata = youtube.get_video_metadata(video_id)
    tracks = youtube.get_youtube_tracks(video_metadata)

    # CHECK
    tracks_check = read_csv("tests/expected_tracks/stephanie_hor_yt_tracks.csv")
    assert tracks["youtube_tracks"] == [track[0] for track in tracks_check]


def test_get_tracks_from_html():
    """Test get youtube tracks from deeper method. Check with docker and local environment."""
    ys = YoutubeManager()
    # LOCAL
    file_off = "page_off.html"
    with open(f"tests/{file_off}") as f:
        page = f.read()
    jtracks_off = ys._get_jtracks_from_html(page)
    tracks_off = ys._get_tracks_from_jtracks(jtracks_off)

    # DOCKER
    file_on = "page_on.html"
    with open(f"tests/{file_on}") as f:
        page = f.read()
    jtracks_on = ys._get_jtracks_from_html(page, test_decode=True)
    tracks_on = ys._get_tracks_from_jtracks(jtracks_on)

    # CHECK
    assert tracks_on == tracks_off
