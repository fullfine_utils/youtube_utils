import json
import os

import openai


def get_dj_from_youtube_title(name):
    openai.api_key = os.environ.get("OPENAI_API_KEY", "")
    messages = [
        {
            "role": "system",
            "content": """
            Accept Input: String representing the title of a YouTube video containing a dj name or multiple dj names.
            Hint: B2B means back to back, and is used to indicate multiple djs playing together.
            JSON Response: Output a JSON response {'dj_list': []} with the names of the djs found in the video.
            """,
        },
        {
            "role": "user",
            "content": json.dumps({"youtube_title": f'"""{name}"""'}),
        },
    ]

    chat = openai.ChatCompletion.create(model="gpt-3.5-turbo", messages=messages)
    reply = chat.choices[0].message.content
    dj_list = json.loads(reply)["dj_list"]
    return dj_list
