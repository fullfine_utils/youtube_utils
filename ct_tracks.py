import json
import os
import re
import time

import openai

from my_utils.myutils import replace_in_line, return_chr_position

# GPT APPROACH


def format_tracks_gpt(input_tracks):
    t1 = time.time()
    openai.api_key = os.environ.get("OPENAI_API_KEY", "")
    messages = [
        {
            "role": "system",
            "content": """
            Accept Input: Process a string list representing raw music tracks data.

            Filter Data: Ignore lines with 'unreleased', 'upcoming', '????', 'tracklist', 'track list', 'setlist', 'id id', or 'unknown unknown'. Case insensitive.

            Clean Data: Clean each line that contains a track, removing any extra or label information (usually in () or []).

            Combine Data: For each track, return a string combining artist and title.

            Double check: Ignore tracks that contain 'unreleased', 'upcoming', '????', 'tracklist', 'track list', 'setlist', 'id id', or 'unknown unknown'. Case insensitive.

            JSON Response: Output a JSON response {'tracks': []} with the cleaned data.

            You aim to provide an efficient, automated, and seamless processing of raw track data, resulting in a clean and user-friendly output.
            """,
        },
        {
            "role": "user",
            "content": json.dumps({"track_list": f'"""{input_tracks}"""'}),
        },
    ]

    chat = openai.ChatCompletion.create(model="gpt-3.5-turbo", messages=messages)
    reply = chat.choices[0].message.content
    tracks = json.loads(reply)["tracks"]
    td = time.time() - t1
    for i in range(len(tracks)):
        tracks[i] = tracks[i].replace("-", "")
        tracks[i] = tracks[i].replace("  ", " ")

    print(f"Time elapsed: {td:.2f} seconds")
    return tracks


# DEFAULT
def format_track_default(x):
    try:
        # REPLACE CHARACTHERS
        line = x.lower()
        line = replace_in_line(
            line,
            " ",
            [" - ", " – ", "=", "feat ", "feat.", "ft ", "ft.", "&"],
        )
        line = " ".join(line.split())

        # DISCARD LINES THAT DOESN'T CONTAIN A TRACK
        discard_list = [
            "unreleased",
            "upcoming",
            "????",
            "tracklist",
            "track list",
            "setlist",
            "id id",
            "unknown unknown",
        ]
        if line == "" or any(d in line for d in discard_list):
            return ""

        # CHECK MINIMUM WORD LENTGTH IS TWO
        line_words = line.split(" ")
        if len(line_words) < 2:
            return None

        # REMOVE INDEX AND TIMESTAMP LOOPING EACH WORD
        line_words.insert(0, "0")
        while line_words[0].isdigit() and line_words[0] != "99999999":
            line_words.pop(0)
            line_words[0] = replace_in_line(
                line_words[0], "", [":", ".", "[", "]", "?", " ", "(", ")"]
            )
        else:
            pass

        # RETRIEVES STRING
        line = " ".join(line_words)

        # REMOVE LABEL INFORMATION
        while line.find("(") > 0:
            p = return_chr_position(line, ["(", ")"])
            not_delete = ["remix", "mix", "edit"]
            if any(d in line[p[0] : p[1] + 1] for d in not_delete):
                line = replace_in_line(line, "", ["(", ")"], 1)
            else:
                line = line[: p[0]] + line[p[1] + 1 :]
        else:
            pass

        # LABEL 2 (REMOVE TEXT BETWEEN [])
        if line.find("[") > 0:
            p = return_chr_position(line, ["[", "]"])
            line = line[: p[0]] + line[p[1] + 1 :]

        # REMOVE TEXT AFTER LAST POINT
        # if line.rfind(".") > 0:
        #     line = line[: line.rfind(".")]

        # DELETE WHITESPACES
        line = " ".join(line.split())

        # DELETE INSTAGRAM TAGS
        if line.find("@") > 0:
            line = line[: line.find("@")]

    except Exception:
        return None

    return line


# REGEX APPROACH
def format_track_re(input_track):
    # Remove unwanted parts from the beginning of the string, especially digits and timestamps.
    # Remove track number or timestamp at the beginning of the line
    song_info = re.sub(r"^[\d:.() -]*", "", input_track.strip(), flags=re.IGNORECASE)

    # If "tracklist", "unreleased", "*ID ID*" or "id id" is found, discard
    patterns_to_discard = r"tracklist|unreleased|\s?\*?ID( -)? ID\*?\s?|\sid( -)? id\s?"
    if re.search(patterns_to_discard, song_info, flags=re.IGNORECASE):
        return ""

    # Remove parentheses that do not contain "mix", "remix", "edit", remove others
    song_info = re.sub(
        r"\((?![^()]*?(mix|remix|edit)[^()]*\))[^()]*\)", "", song_info, flags=re.IGNORECASE
    )
    # Remove label information in square brackets
    song_info = re.sub(r"\[.*?\]", "", song_info)
    # Remove @username
    song_info = re.sub(r"@\w+", "", song_info).strip()
    # Remove -
    song_info = re.sub(r" - ", " ", song_info).strip()
    # Remove only ()
    song_info = song_info.replace("(", "")
    song_info = song_info.replace(")", "")
    # Remove &
    song_info = song_info.replace("&", "")

    # If the line is not empty, return it; otherwise, return an empty string
    return song_info if song_info else ""


# description_tracks = video_metadata["description"].split("\n")
# description_tracks = description_tracks[11:30]

# description_tracks = [
# "00:00:20 - Modig - The Way It Breaks",
# "00:04:13 - Sleeps Everywhere - Extinguish [EMERALD004]",
# "00:09:00 - DisX3 - Stigma [ICR013]",
# "00:14:08 - Milier Avenue - CY-BM V8 [AKKOET]",
# "00:16:48 - DJ Hell - Allerseelen (Jeff Mills Remix)",
# "00:20:54 - Robert Natus - Pain",
# "00:26:02 - Brixton - Fuckass (Robert Natus Remix)",
# "00:30:08 - Gaetano Verdi & Dorian Hunter & Kamil Marc - Damage",
# "00:33:49 - Titanium - Tuxedo (Patrik Skoog Remix)",
# "00:36:25 - Kay D Smith - Person called (Original Mix)",
# ]

# description_tracks = [
# "01. Amelie Lens - Man over Machine (coming soon on Lenske)",
# "02. Frazier - Unreleased",
# "03. Amelie Lens - Storm (coming soon on Lenske)",
# "04. Giovanni Carozza - Closer (JAM)",
# "05. Farrago - Sinner ",
# "06. Flug - Unreleased",
# "07. Milo Spykers - Zenith",
# "08. A*S*Y*S - Bassface  (Suara)",
# "09. Tobias Lueke - Unreleased",
# "10. Unreleased, upcoming Lenske",
# "11. Klangkuenstler - unreleased",
# "12. Peder Mannerfelt ft. Sissel Wincent - Sissel & Bass (Perc Remix) (Peder Mannerfeld Production)",
# "13. Blue Hour - BLUEHOURMX 004 A1 Solace (Héctor Oaks To The Core Mix) D1644 (Blue Hour)",
# "14. Regal - Unreleased",
# "15. Unreleased, upcoming Lenske",
# "16. Klangkuenstler - Razor",
# "17. Perc - Look What Your Love Has Done To Me (Amelie Lens Remix)",
# "18. Clouds - Chained To A Dead Camel (Overlee Assembyl)",
# "19. Rommwick - Es Schallt Die Ewigkeit (Gonzo MDF Remix) (Obscuur Records)",
# "20. Shdw & Obscure Shape - Leiser Zorn (Arts core)",
# "21. Unreleased, upcoming Lenske",
# "22. Tobias Lueke - Bound (Eclipse Recordings)",
# "23. Unreleased",
# "24. DurtysoxXx & Optimuss - Aberrant (ODD Recordings)",
# "25. Mehen - Avalanches Of Compromise ",
# "26. Trym- Sparkling System (Involve Records)",
# ]

# import re

# import re

# def clean_song_title(title):
#     # Remove everything after '@'
#     title = re.sub(r'@.*$', '', title)

#     # Remove everything after '['
#     title = re.sub(r'\[.*$', '', title)

#     # Remove everything after '(' if there is no 'original', 'mix', 'edit', 'remix' inside
#     title = re.sub(r'\((?!.*?(original|mix|edit|remix)).*?\)', '', title, flags=re.IGNORECASE)

#     # Remove initial track number and timestamp block
#     title = re.sub(r'^\d+[^a-zA-Z]*', '', title)

#     return title.strip()  # Remove any leading/trailing white spaces


# dt_result = []
# for line in description_tracks:
#     dt_result.append(clean_song_title(line))
